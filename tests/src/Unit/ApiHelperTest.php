<?php

namespace Drupal\Tests\forecast_solar\Unit;

use Drupal\forecast_solar\Services\ApiHelper\ApiHelper;
use Drupal\forecast_solar\Services\ApiHelper\ApiHelperInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Test ApiHelper.
 *
 * @coversDefaultClass \Drupal\forecast_solar\Services\ApiHelper\ApiHelper
 * @group forecast_solar
 */
class ApiHelperTest extends UnitTestCase {

  /**
   * The API-helper.
   *
   * @var \Drupal\forecast_solar\Services\ApiHelper\ApiHelperInterface
   */
  protected ApiHelperInterface $apiHelper;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->apiHelper = new ApiHelper();
  }

  /**
   * Tests the ::convertPeriodicData method.
   *
   * @param mixed $input
   *   The input data.
   * @param mixed $expected
   *   The expected result.
   *
   * @dataProvider periodicDataProvider
   */
  public function testConvertPeriodicData($input, $expected): void {
    $output = $this->apiHelper::convertPeriodicData($input);
    $this->assertEquals($expected, $output);
  }

  /**
   * Data-provides for testConvertPeriodicData.
   *
   * @return \Generator
   *   The data records for testing.
   */
  public function periodicDataProvider(): \Generator {
    $data = $this->randomString();
    yield [
      $data,
      $data,
    ];

    $data = [$this->randomString()];
    yield [
      $data,
      $data,
    ];

    $data = [$this->randomString() => $this->randomString()];
    yield [
      $data,
      $data,
    ];

    $data = [date('Y-m-d') => rand(0, 100)];
    yield [
      $data,
      $data,
    ];

    $data = [date('Y-m-d H:i:s') => rand(0, 100)];
    $keys = array_keys($data);
    yield [
      $data,
      [
        'original' => $data,
        'values' => [
          [
            'period_start' => $keys[0],
            'value' => $data[$keys[0]],
          ],
        ],
      ],
    ];
  }

}
