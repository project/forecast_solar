<?php

namespace Drupal\forecast_solar\Services\ApiHelper;

use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Provides a collection of API-helper methods.
 */
class ApiHelper implements ApiHelperInterface {

  /**
   * {@inheritdoc}
   */
  public static function convertPeriodicData($input) {
    if (!is_array($input)) {
      return $input;
    }

    // Check for a valid date.
    $keys = array_keys($input);
    try {
      $date = DrupalDateTime::createFromFormat('Y-m-d H:i:s', reset($keys), NULL, ['langcode' => 'en']);
      if ($date->hasErrors()) {
        return $input;
      }
    }
    catch (\Exception $e) {
      return $input;
    }

    $data = [
      'original' => $input,
      'values' => [],
    ];
    foreach (array_keys($input) as $key) {
      $data['values'][] = [
        'period_start' => $key,
        'value' => $input[$key],
      ];
    }

    return $data;
  }

}
