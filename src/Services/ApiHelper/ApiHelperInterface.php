<?php

namespace Drupal\forecast_solar\Services\ApiHelper;

/**
 * Provides an interface for API-helper methods.
 */
interface ApiHelperInterface {

  /**
   * Convert a collection of periodic data.
   *
   * @param mixed $input
   *   The data to convert.
   *
   * @return mixed
   *   Returns the altered input when applicable.
   */
  public static function convertPeriodicData($input);

}
