# Forecast.Solar

Restful API for Solar production forecast data and Weather forecast data.

For the official page of the module, visit the
[project page](https://www.drupal.org/project/forecast_solar).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/forecast_solar).

## Table of contents

- Requirements
- Installation
- Configuration
- Usage
- Remarks
- Maintainers

## Requirements

- [HTTP Client Manager](https://drupal.org/project/http_client_manager)
  - For supporting the service descriptions

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Navigate to Administration > Configuration > Web services > HTTP Client Manager > [YAML] Forecast Solar API (`/admin/config/services/http-client-manager/forecast_solar_services`) and configure requests as you see fit.

## Usage

See [HTTP Client Manager](https://drupal.org/project/http_client_manager).

## Remarks

The output of the estimate-endpoint (`/estimate`) contains dynamic keys (datetime-strings) for the estimated values. This is something that can not be described by Guzzle. To circumvent this, there's a helper-method being used as a filter:
```yaml
watts:
  type: object
  filters:
    - Drupal\forecast_solar\Services\ApiHelper\ApiHelper::convertPeriodicData
```

This ensure that
```json
{
  "watts": {
    "2024-04-12": 13969,
    "2024-04-13": 11420
  }
}
```
Becomes this
```json
{
  "watts": {
    "original": {
      "2024-04-12 15:00:00": 13969,
      "2024-04-12 16:00:00": 11420
    },
    "values": [
      {
        "period_start": "2024-04-12 15:00:00",
        "value": 13969
      },
      {
        "period_start": "2024-04-12 16:00:00",
        "value": 11420
      }
    ]
  }
}
```

## Maintainers

- Jasper Lammens - [@lammensj](https://www.drupal.org/u/lammensj)
